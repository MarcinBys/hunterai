﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneGenerator : MonoBehaviour {

    // <<< Prefabs to spawn >>>
    public GameObject tree;    
    public GameObject hunter;
    public GameObject deer;

    // <<< Amount of prefabs to spawn >>>
    private int treesAmount;
    [Range(0, 10)] public int huntersAmount;            // debug purpose only
    private int deersAmount;


    void Awake()
    {
        GenerateObjects(int.Parse(PlayerPrefs.GetFloat("Trees amount").ToString()), tree);
        GenerateObjects(huntersAmount, hunter);
        GenerateObjects(int.Parse(PlayerPrefs.GetFloat("Deers amount").ToString()), deer);
    }


    /// <summary>
    /// Object generator in random positions in camera view (need to change Random.Range values if camera is set further)
    /// </summary>
    /// <param name="objectAmount">Amount of objects to spawn.</param>
    /// <param name="objectToSpawn">Prefab to spawn.</param>
    public void GenerateObjects(int objectAmount, GameObject objectToSpawn)
    {
        for (int i = 0; i < objectAmount; i++)
        {
            Vector3 objectPosition = new Vector3(Random.Range(-66, 66), Random.Range(-35, 35), 0);

            if (!Physics2D.OverlapCircle(objectPosition, 3))    // if there is nothing at this position
            {
                Instantiate(objectToSpawn, objectPosition, new Quaternion());
            }
            else                                                // if there already exists something in this position
            {
                i--;
            }
        }
    }
}