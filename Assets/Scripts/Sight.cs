﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sight : MonoBehaviour {
    
    private HunterController hunter;
    private BoxCollider2D sight;

    // <<< SIGHT >>>                                // to set in inspector
    public Vector2 normalSight;                     // recommended default: (8,6)
    public Vector2 trackingSight;                   // recommended default: (1,6)

    private void Start()
    {
        hunter = GetComponentInParent<HunterController>();
        sight = GetComponent<BoxCollider2D>();

        SetSightSize(true);                        // set starting sight size to normal
    }

    
    private void OnTriggerEnter2D(Collider2D other)
    {
        // if sight collider triggers track collider and track not already found, then start tracking it
        if (other.CompareTag("Track") && !other.gameObject.GetComponent<TrackDestroyer>().trackFound)
        {
            hunter.StartTracking(other);

            hunter.IncreaseTracksCounter();                                         // increase found tracks counter for statistics purpose 
            other.gameObject.GetComponent<TrackDestroyer>().trackFound = true;      // set it to already found
        }
    }


    /// <summary>
    /// Set hunter's sight collider size.
    /// </summary>
    /// <param name="normal">true = normal size, false = tracking size</param>
    public void SetSightSize(bool normal)
    {
        if (normal)
        {
            sight.size = normalSight;
        }
        else
        {
            sight.size = trackingSight;
        }
    }
}
