﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {

    [SerializeField] private Transform eyeLine;
    [SerializeField] private LayerMask visibleObjects;

    private AudioSource audioSrc;
    private LineRenderer line;                                  // need for drawing hunter laser's line
    private HunterController hunter;

    [SerializeField] private float shootRange;                  // recommended default: 25
    [SerializeField] private Transform endEyeLine;              // laser end position
    private Hear hear;

    // <<< RELOADING >>>
    /// <summary>
    /// Time needed to reload a weapon. Set by user.
    /// </summary>
    private float reloadTime;
    /// <summary>
    /// Holds current reloading time
    /// </summary>
    private float reloadTimer;
    private bool isReloading;


    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
        hear = GetComponentInChildren<Hear>();
        line = GetComponent<LineRenderer>();
        hunter = GetComponent<HunterController>();

        reloadTime = PlayerPrefs.GetFloat("Reload time");
        isReloading = false;
        reloadTimer = 0f;
        line.positionCount = 2;                                 // laser table size (2 vectors)
    }


    void Update()
    {
        if (!isReloading)
        {
            TakeAim();
        }
        else if (isReloading)
        {
            Reloading();
        }
    }


    /// <summary>
    /// Aiming function.
    /// </summary>
    private void TakeAim()
    {
        var hit = Physics2D.Raycast(eyeLine.position, eyeLine.TransformDirection(Vector2.up), shootRange, visibleObjects);

        // spawn laser line from eyeLine to endEyeLine position (Debug.DrawRay is not visible on user's scene)
        line.SetPosition(0, eyeLine.position);
        line.SetPosition(1, endEyeLine.position);
        //Debug.DrawRay(eyeLine.position, eyeLine.TransformDirection(Vector2.up) * shootRange, Color.red, 0.1f);
        
        if (hit && hit.collider.gameObject.CompareTag("Deer"))
        {
            ShootDeer(hit);                                                         // kill the deer
            hear.IncreaseHearRange(PlayerPrefs.GetFloat("Hear range bonus"));       // reward Hunter ("level up")
            hunter.SetRegressTimer();                                               // set regressTime to 0 s.
        }
    }


    /// <summary>
    /// Shoot deer on sight.
    /// </summary>
    private void ShootDeer(RaycastHit2D hit)
    {
        audioSrc.Play();    // play shoot sound
        hit.collider.gameObject.GetComponent<DeerController>().Death(hear.GetHearRange());

        isReloading = true;
        reloadTimer = 0;

        hunter.IncreaseKillsCounter();
    }


    private void Reloading()
    {
        hunter.SetCurrentState("RELOADING");

        line.positionCount = 0;                             // delete laser line

        reloadTimer += Time.deltaTime;

        if (reloadTimer >= reloadTime)
        {
            isReloading = false;
            line.positionCount = 2;                         // restore laser table size
        }
    }
}