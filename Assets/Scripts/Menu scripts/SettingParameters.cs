﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingParameters : MonoBehaviour {

    // <<< Statistics field object >>>
    public InputField statistics;

    // <<< Slider objects >>>
    public Slider deersAmount;
    public Slider treesAmount;
    public Slider simulationTime;
    public Slider deerStartingSpeed;
    public Slider hunterStartingSpeed;
    public Slider hunterReloadTime;
    public Slider hunterRegressTime;
    public Slider hunterHearRangeBonus;
    public Slider hunterHearRangePunish;

    /// <summary>
    /// Title texts from: deersAmount, treesAmount, simulationTime, deerStartingSpeed, hunterStartingSpeed, hunterReloadTime, hunterRegressTime, hunterHearRangeBonus, hunterHearRangePunish
    /// </summary>
    private Slider[] sliders;
    

    private void Start()
    {
        sliders = new Slider[9] { deersAmount, treesAmount, simulationTime,
                                  deerStartingSpeed,
                                  hunterStartingSpeed, hunterReloadTime, hunterRegressTime, hunterHearRangeBonus, hunterHearRangePunish };
        
        LoadAll();

        // Load saved statistics from PlayerPrefs and display them in InputField
        try
        {
            statistics.text = PlayerPrefs.GetString("Statistics");
        }
        catch
        {
            Debug.Log("Klucz 'Statistics' jeszcze nie istnieje.");
        }
    }


    /// <summary>
    /// Saves all values set by user by sliders.
    /// </summary>
    public void SaveAll()
    {
        foreach (Slider slider in sliders)
        {
            SaveValue(slider.GetComponentInChildren<Text>().text, slider.value);
        }
    }


    /// <summary>
    /// Loads all values from PlayerPrefs to slider's value (from "sliders" array).
    /// </summary>
    public void LoadAll()
    {
        try
        {
            foreach (Slider slider in sliders)
            {
                slider.value = LoadValue(slider.GetComponentInChildren<Text>().text);
            }
        }
        catch
        {
            Debug.Log("Klucze nie istnieją.");
        }
    }


    /// <summary>
    /// Saves value to key and keeps it in PlayerPrefs.
    /// </summary>
    /// <param name="name">key name</param>
    /// <param name="value">value to assign</param>
    public void SaveValue(string name, float value)
    {
        PlayerPrefs.SetFloat(name, value);
    }


    /// <summary>
    /// Load value from key.
    /// </summary>
    /// <param name="name">Key name</param>
    /// <returns>Value assigned to key</returns>
    public float LoadValue(string name)
    {
        return PlayerPrefs.GetFloat(name);
    }


    /// <summary>
    /// Load scene with ID number.
    /// </summary>
    /// <param name="sceneID">Scene's ID number</param>
    public void LoadScene(int sceneID)
    {
        SceneManager.LoadScene(sceneID);
    }


    /// <summary>
    /// Quit from application.
    /// </summary>
    public void Quit()
    {
        Application.Quit();
    }
}