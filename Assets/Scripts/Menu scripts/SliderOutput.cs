﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderOutput : MonoBehaviour {

    private Slider slider;
    public Text value;


    private void Start()
    {
        slider = GetComponent<Slider>();
    }


    void Update () {
        // show slider's value in text field
        value.text =  slider.value.ToString("F2");
	}
}