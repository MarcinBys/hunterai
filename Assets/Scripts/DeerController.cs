﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeerController : MonoBehaviour {

    // <<< DEER VARIABLES >>>
    private float startingSpeed;                        // deer's movement speed
    private float currentSpeed;                         // for saving initial deer's speed
    private float runningSpeed;                         // used to calculate running speed from starting speed
    private bool isRunningAway;                         // check if deer is in running away state
    private float collisionTimer;                       // need for preventing hanging on walls/trees for longer
    private string currentState;                        // set object's current state for displaying

    // <<< TRACKS HANDLING >>>
    private List<GameObject> allTracks;                 // list with all spawned tracks (need for destroying tracks when deer is dead)
    public GameObject track;                            // track prefab
    private Vector3 lastTrackPos;                       // used in counting distance to the last track

    // <<< DEATH HANDLING >>>
    private AudioSource audioSrc;
    private Renderer rend;
    private CapsuleCollider2D col;
    private bool isAlive;
    private SceneGenerator sceneGenerator;              // for spawning new deer upon death

    // <<< STATISTICS >>>
    private GameController gameController;

    /// <summary>
    /// [timeAlive, hunterHearRange, tracksCount]
    /// </summary>
    public float[] deerData = new float[3] { 0f, 0f, 0f };


    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
        isAlive = true;
        allTracks = new List<GameObject>();                             // tracks list initialization

        // Statistics
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        SetCurrentState("Wandering");                                   // set current state (visible in UI)

        // Speeds
        startingSpeed = PlayerPrefs.GetFloat("Deers starting speed");   // load deer staring speed
        currentSpeed = startingSpeed;                                   // set current speed as starting speed
        runningSpeed = startingSpeed * 3;                               // calculate running speed
        isRunningAway = false;                                          // set deer's state as not running away
        
        // Set starting behaviour
        InvokeRepeating("ChangeDirection", 5f, 5f);                     // start ChangeDirection (after x sec) and repeat every x secs
        transform.Rotate(Vector3.forward * Random.Range(-180, 180));    // random object rotation on simulation start
        LeaveTrack();                                                   // leave the first track
    }


    void Update()
    {
        if (isAlive)
        {
            deerData[0] += Time.deltaTime;              // count deer's lifetime
        }
    }


    void FixedUpdate()
    {
        if (isAlive)
        {
            Movement();
        }
    }


    #region Deer's behaviour

    /// <summary>
    /// Simulate deer movement. Deer moves by his own up vector.
    /// </summary>
    private void Movement()
    {
        transform.position += transform.up * Time.deltaTime * currentSpeed;

        // Leave the track if the distance from the last one is >= 4
        if (Vector3.Distance(transform.position, lastTrackPos) >= 4)
        {
            LeaveTrack();
        }

        if (!isRunningAway)
        {
            SetCurrentState("WANDERING");
        }
    }


    /// <summary>
    /// Change object rotation (direction where he is moving to) randomly (from -90 to 90)
    /// </summary>
    private void ChangeDirection()
    {
        transform.Rotate(Vector3.forward * Random.Range(-90, 90));

        // leave the track when direction changed
        LeaveTrack();
    }


    /// <summary>
    /// Spawn new track object at deer's location and adds it to list. Save last track's position to a variable.
    /// </summary>
    private void LeaveTrack()
    {
        GameObject newTrack = Instantiate(track, transform.position, transform.rotation);
        allTracks.Add(newTrack);

        deerData[2]++;

        lastTrackPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }


    /// <summary>
    /// Running away behaviour.
    /// </summary>
    /// <param name="other"></param>
    public void RunAway(Collider2D other)
    {
        if (!isRunningAway)
        {
            currentSpeed = runningSpeed;
            isRunningAway = true;
        }
        
        // rotate away from the hunter
        //Vector3 dir = other.transform.position - transform.position;        // direction to look at
        //float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;            // direction conversion to angle (in degrees)
        //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0f, 0f, angle - 270), currentSpeed * Time.deltaTime);

        if (isAlive)
        {
            SetCurrentState("FLEEING");
        }
    }


    /// <summary>
    /// Cancel run away behaviour.
    /// </summary>
    public void CancelRunAway()
    {
        isRunningAway = false;
        currentSpeed = startingSpeed;

        SetCurrentState("WANDERING");
    }


    /// <summary>
    /// Destroy deer and all his tracks.
    /// </summary>
    public void Death(float hunterHearRangeRadius)
    {
        // desactivate main object's components and functions (to be able to hear only the death sound)
        isAlive = false;
        CancelInvoke("ChangeDirection");
        rend = GetComponent<Renderer>();
        rend.enabled = false;
        col = GetComponent<CapsuleCollider2D>();
        col.enabled = false;
        CancelRunAway();

        // Statistics
        SetCurrentState("DEAD");
        deerData[1] = hunterHearRangeRadius;
        gameController.AddDeerData(deerData);

        // play death sound
        audioSrc.Play();

        // destroy all tracks from the allTracks list
        foreach (GameObject track in allTracks)
        {
            Destroy(track);
        }

        // spawn new deer
        sceneGenerator = GameObject.FindWithTag("SceneGenerator").GetComponent<SceneGenerator>();
        sceneGenerator.GenerateObjects(1, sceneGenerator.deer);

        // destroy deer object after 3 seconds
        Destroy(gameObject, 3f);
    }

    #endregion


    #region Collisions Handling
    private void OnCollisionEnter2D(Collision2D other)
    {
        // if a deer collides with a tree, change his rotation randomly either by angle -90 or 90
        if (other.collider.CompareTag("Tree") || other.collider.CompareTag("Wall"))
        {
            collisionTimer = 0;
            
            int angle = Random.Range(0, 1);

            if (angle.Equals(0))            // turn left
            {
                transform.Rotate(Vector3.forward * -95);
            }
            else if (angle.Equals(1))       // turn right
            {
                transform.Rotate(Vector3.forward * 95);
            }
        }
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        collisionTimer += Time.deltaTime;

        if ((other.collider.CompareTag("Tree") || other.collider.CompareTag("Wall")) && collisionTimer >= 0.2f)
        {
            int angle = Random.Range(0, 1);

            if (angle.Equals(0))            // turn left
            {
                transform.Rotate(Vector3.forward * -91);
            }
            else if (angle.Equals(1))       // turn right
            {
                transform.Rotate(Vector3.forward * 91);
            }
        }
    }
    
    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.collider.CompareTag("Tree") || other.collider.CompareTag("Wall"))
        {
            collisionTimer = 0;
            LeaveTrack();               // leave a track when unstucked
        }
    }
    #endregion


    #region Statistics display
    
    /// <summary>
    /// Returns deer's time alive.
    /// </summary>
    public float GetTimeAlive()
    {
        return Mathf.Round(deerData[0]);
    }

    /// <summary>
    /// Returns deer's tracks left counter.
    /// </summary>
    /// <returns></returns>
    public float GetTracksLeftCount()
    {
        return deerData[2];
    }
    
    /// <summary>
    /// Return current deer's state.
    /// </summary>
    /// <returns></returns>
    public string GetCurrentState()
    {
        return currentState;
    }

    /// <summary>
    /// Set current state.
    /// </summary>
    public void SetCurrentState(string state)
    {
        currentState = state;
    }

    #endregion
}