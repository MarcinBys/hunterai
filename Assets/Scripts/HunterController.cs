﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HunterController : MonoBehaviour {

    public Transform currentTrackTransform;             // holds last seen track's transform
    private Sight sight;                                // used to get object's child sight component

    private bool tracking = false;                       // need for checking if hunter is tracking at the moment

    private float collisionTimer;                       // time to unstuck when hitting other collider

    // <<< SPEED >>>
    private float startingSpeed;                        // hunter's movement speed
    private float currentSpeed;                         // for saving initial hunter's speed
    private float trackingSpeed;                        // used to calculate tracking speed from starting speed

    // <<< Hear range variables >>>
    private Hear hear;
    private float regressTime;                          // set by user, decrease hear range if hunter didn't kill anything for x time
    private float regressTimer;                         // holds time to regress

    // <<< STATISTICS >>>
    private int killsCounter;
    private int tracksCounter;
    private int hearingCounter;
    private string currentState;


    /// <summary>
    /// Initialization variables.
    /// </summary>
    void Start ()
    {
        SetCurrentState("SEEKING");                                     // set state on startup
        
        sight = gameObject.GetComponentInChildren<Sight>();             // get children's sight component
        hear = gameObject.GetComponentInChildren<Hear>();               // get children's hear component

        transform.Rotate(Vector3.back * Random.Range(-180, 180));       // random object rotation on simulation start

        // Set speeds
        startingSpeed = PlayerPrefs.GetFloat("Starting speed");
        currentSpeed = startingSpeed;                                   // set current speed as starting speed
        trackingSpeed = startingSpeed * 1.1f;                           // calculate running speed

        // Set regress timers
        regressTimer = 0f;
        regressTime = PlayerPrefs.GetFloat("Regress time");
        
        // Statistics initialization
        killsCounter = 0;
        tracksCounter = 0;
        hearingCounter = 0;
        
        // Start hunter's behaviour
        StartSeeking();
    }


    void Update()
    {
        Regress();
    }


    void FixedUpdate ()
    {
        Movement();
	}


    #region Hunter Controlling

    /// <summary>
    /// Simulate hunter movement.
    /// </summary>
    private void Movement()
    {
        // if the hunter is tracking but doesn't have next track in his sight, then stop tracking and return to deer seeking
        if (IsTracking() && (currentTrackTransform.Equals(null) || transform.position.Equals(currentTrackTransform.position)) )
        {
            StopTracking();
            StartSeeking();
        }

        // if the hunter is tracking and found next track but did not reached it's position
        else if (IsTracking() && !transform.position.Equals(currentTrackTransform.position) )
        {
            transform.position = Vector3.MoveTowards(transform.position, currentTrackTransform.position, currentSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, currentTrackTransform.rotation, currentSpeed * Time.deltaTime);

            SetCurrentState("TRACKING");
        }
        // else if the hunter is not tracking (regular movement)
        else if (!IsTracking())
        {
            transform.position += transform.up * Time.deltaTime * currentSpeed;
            SetCurrentState("SEEKING");
        }
    }


    /// <summary>
    /// Change object rotation (direction where he is moving to) randomly (from -90 to 90)
    /// </summary>
    public void ChangeDirection()
    {
        transform.Rotate(Vector3.back * Random.Range(-90, 90));
    }


    /// <summary>
    /// Start hunter's normal behaviour when not tracking.
    /// </summary>
    private void StartSeeking()
    {
        currentSpeed = startingSpeed;

        // start ChangeDirection (after 5 sec) and repeat every 5 secs
        InvokeRepeating("ChangeDirection", 5f, 5f);
    }


    /// <summary>
    /// Stop hunter's normal behaviour.
    /// </summary>
    public void StopSeeking()
    {
        // cancel last InvokeRepeaking
        CancelInvoke("ChangeDirection");
    }


    /// <summary>
    /// Start tracking behaviour when the track is spotted.
    /// </summary>
    /// <param name="track">Spotted track's collider</param>
    public void StartTracking(Collider2D track)
    {
        StopSeeking();

        currentSpeed = trackingSpeed;

        sight.SetSightSize(false);                              // set sight collider size to smaller (0.5,2)
        currentTrackTransform = track.gameObject.transform;     // save spotted track's transform to variable

        tracking = true;
    }


    /// <summary>
    /// Stop tracking behaviour.
    /// </summary>
    public void StopTracking()
    {
        sight.SetSightSize(true);                  // set sight collider size back to normal (5,3)

        tracking = false;
    }


    /// <summary>
    /// Check if the hunter is tracking.
    /// </summary>
    /// <returns>Returns true when the hunter is tracking.</returns>
    private bool IsTracking()
    {
        return tracking;
    }

    
    /// <summary>
    /// Rotate towards object (for example aim at the deer).
    /// </summary>
    /// <param name="other">Object's collider</param>
    public void HearingMovement(Collider2D other)
    {
        Vector3 dir = other.transform.position - transform.position;        // direction to look at
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;            // direction conversion to angle (in degrees)
        
        // rotate towards deer with regular speed
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0f, 0f, angle - 90), currentSpeed * Time.deltaTime);

        SetCurrentState("AIMING");
    }


    /// <summary>
    /// Set timer holding current time before regress.
    /// </summary>
    /// <param name="value"></param>
    public void SetRegressTimer(float value = 0)
    {
        regressTimer = value;
    }


    /// <summary>
    /// Handles regress time mechanics.
    /// </summary>
    private void Regress()
    {
        regressTimer += Time.deltaTime;

        if (regressTimer >= regressTime)
        {
            SetRegressTimer(0f);
            hear.DecreaseHearRange(PlayerPrefs.GetFloat("Hear range punish"));
        }
    }
    
    #endregion

    #region Statistics

    /// <summary>
    /// Return current kills counter.
    /// </summary>
    public int GetKillsCounter()
    {
        return killsCounter;
    }


    /// <summary>
    /// Increase kills counter by 1.
    /// </summary>
    public void IncreaseKillsCounter()
    {
        killsCounter += 1;
    }


    /// <summary>
    /// Return tracks found by hunter counter.
    /// </summary>
    public int GetTracksCounter()
    {
        return tracksCounter;
    }


    /// <summary>
    /// Increase tracks found counter by 1.
    /// </summary>
    public void IncreaseTracksCounter()
    {
        tracksCounter += 1;
    }


    /// <summary>
    /// Return (so far) hearing counter.
    /// </summary>
    public int GetHearingCounter()
    {
        return hearingCounter;
    }


    /// <summary>
    /// Increase hearing counter by 1.
    /// </summary>
    public void IncreaseHearingCounter()
    {
        hearingCounter += 1;
    }


    /// <summary>
    /// Return current hunter's hear range.
    /// </summary>
    public float GetHearRange()
    {
        return hear.GetHearRange();
    }


    /// <summary>
    /// Return current hunter's state.
    /// </summary>
    public string GetCurrentState()
    {
        return currentState;
    }


    /// <summary>
    /// Set current state.
    /// </summary>
    public void SetCurrentState(string state)
    {
        currentState = state;
    }
    
    #endregion

    #region Collision Handling

    private void OnCollisionEnter2D(Collision2D other)
    {
        // if a deer collides with a tree, change his rotation randomly either by angle -95 or 95
        if ((other.collider.CompareTag("Tree") || other.collider.CompareTag("Wall")) && !IsTracking())
        {
            collisionTimer = 0;

            int angle = Random.Range(0, 1);

            if (angle.Equals(0))
            {
                transform.Rotate(Vector3.back * -95);
            }
            else if (angle.Equals(1))
            {
                transform.Rotate(Vector3.back * 95);
            }
        }
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        collisionTimer += Time.deltaTime;               // count how many time spent in collision (stuck)

        if ((other.collider.CompareTag("Tree") || other.collider.CompareTag("Wall")) && collisionTimer >= 0.2f && !IsTracking())
        {
            ChangeDirection();
        }
    }
    
    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.collider.CompareTag("Tree") || other.collider.CompareTag("Wall"))
        {
            collisionTimer = 0;
        }
    }
    #endregion
}