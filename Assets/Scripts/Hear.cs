﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hear : MonoBehaviour {

    private CircleCollider2D hearCollider;
    private HunterController hunter;
    private DeerController deer;
    
    // <<< RANGES >>>
    private float minHearRange;
    private float maxHearRange;

    /// <summary>
    /// Initialize hear variables.
    /// </summary>
    void Start () {
        hearCollider = GetComponent<CircleCollider2D>();

        // Get proper component
        if (transform.parent.CompareTag("Hunter"))
        {
            hunter = GetComponentInParent<HunterController>();
        }
        else if (transform.parent.CompareTag("Deer"))
        {
            deer = GetComponentInParent<DeerController>();
        }        

        // Initialize min and max hear range (FOR HUNTER)
        minHearRange = 10f;
        maxHearRange = 20f;
    }
    

    /// <summary>
    /// Increase hear range by value.
    /// </summary>
    /// <param name="value">Bonus hear range value</param>
    public void IncreaseHearRange(float value)
    {
        if (hearCollider.radius < maxHearRange)
        {
            hearCollider.radius += value;
        }
    }


    /// <summary>
    /// Decrease hear range by value.
    /// </summary>
    /// <param name="value">Punish hear range value</param>
    public void DecreaseHearRange(float value)
    {
        if (hearCollider.radius > minHearRange)
        {
            hearCollider.radius -= value;
        }
    }


    /// <summary>
    /// Get object's current hear range radius.
    /// </summary>
    /// <returns> Hear collider radius</returns>
    public float GetHearRange()
    {
        return hearCollider.radius;
    }


    #region Collision handling

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Deer") && gameObject.transform.parent.CompareTag("Hunter"))           // if hunter hears deer
        {
            hunter.StopTracking();
            hunter.HearingMovement(other);
        }
        else if (other.CompareTag("Hunter") && gameObject.transform.parent.CompareTag("Deer"))      // if deer hears hunter
        {
            deer.RunAway(other);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Hunter") && gameObject.transform.parent.CompareTag("Deer"))           // if deer stops hearing hunter
        {
            deer.CancelRunAway();
        }
        else if (other.CompareTag("Deer") && gameObject.transform.parent.CompareTag("Hunter"))      // if hunter hears a deer
        {
            hunter.IncreaseHearingCounter();
        }
    }
    #endregion
}