﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    /// <summary>
    /// List of data of each deer killed: [life time, hunter hear range, tracks count]
    /// </summary>
    public List<float[]> deerDataList;

    private GameObject selectedObject;          // keep selected (by click) object in variable

    // <<< SIMULATION TIME >>>
    private float simulationTime;
    private float simulationTimer;

    // <<< UI >>>
    public Text simulationTimeText;
    public Text objectType;
    public Text state;
    public Text stat1;
    public Text stat2;
    public Text stat3;
    public Text stat4;


    void Start ()
    {
        simulationTimeText.text = "";
        simulationTime = PlayerPrefs.GetFloat("Simulation time");

        deerDataList = new List<float[]> { };

        selectedObject = GameObject.FindGameObjectWithTag("Hunter");        // default selected object on simulation start is a hunter
    }


    private void Update()
    {
        HandleSimulationTime();

        SelectCharacter();
    }


    /// <summary>
    /// Handle simulation time and end.
    /// </summary>
    private void HandleSimulationTime()
    {
        // Set simulation time
        if (simulationTime >= 1)
        {
            simulationTimer += Time.deltaTime;

            if (simulationTimer >= simulationTime)
            {
                // Go to the summary screen (menu)
                SaveStatistics();
                LoadScene(0);
            }

            simulationTimeText.text = Mathf.Round(simulationTime - simulationTimer).ToString();
        }
    }


    /// <summary>
    /// Character (hunter / deer) selection method.
    /// </summary>
    private void SelectCharacter()
    {
        // Set selectedObject back to Hunter when the deer is killed
        if (selectedObject.Equals(null))
        {
            selectedObject = GameObject.FindGameObjectWithTag("Hunter");
        }
        
        // Get object by selecting it with left mouse button
        try
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit2D hit = Physics2D.GetRayIntersection(Camera.main.ScreenPointToRay(Input.mousePosition));

                if (!hit.Equals(null) && (hit.collider.CompareTag("Hunter") || hit.collider.CompareTag("Deer")))
                {
                    selectedObject = hit.collider.gameObject;
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
        
        // Refresh data on display panel
        PanelDisplayData(selectedObject);
    }


    /// <summary>
    /// Add (preferably killed) deer's table with data to list.
    /// </summary>
    /// <param name="data">[life time, hunter hear range, tracks count]</param>
    public void AddDeerData(float[] data)
    {
        deerDataList.Add(data);
    }


    /// <summary>
    /// Count average value from objects in deerDataList.
    /// </summary>
    /// <param name="tableValue">Value from deerDataList</param>
    /// <returns>Average value</returns>
    private float CountAverage(int tableValue)
    {
        if (deerDataList.Count.Equals(0))
        {
            return 0;
        }
        try
        {
            return CountSum(tableValue) / deerDataList.Count;
        }
        catch
        {
            Debug.Log("Nie mozna dzielic przez 0.");
            return 0f;
        }
    }


    /// <summary>
    /// Count sum from selected objects value in deerDataList.
    /// </summary>
    /// <param name="tableValue">Value from deerDataList</param>
    /// <returns>Sum</returns>
    private float CountSum(int tableValue)
    {
        if (deerDataList.Count.Equals(0))
        {
            return 0;
        }
        try
        {
            float sum = 0f;

            for (int i = 0; i < deerDataList.Count; i++)
            {
                sum += deerDataList[i][tableValue];
            }

            return sum;
        }
        catch
        {
            Debug.Log("Blad.");
            return 0f;
        }
    }


    /// <summary>
    /// Display data of selected gameobject (hunter or deer).
    /// </summary>
    /// <param name="obj">Selected object</param>
    private void PanelDisplayData(GameObject obj)
    {
        if (obj.CompareTag("Hunter"))
        {
            HunterController hc = obj.GetComponent<HunterController>();

            objectType.text = obj.tag;
            state.text = hc.GetCurrentState();
            stat1.text = "Heard: " + hc.GetHearingCounter().ToString();
            stat2.text = "Killed: " + hc.GetKillsCounter().ToString();
            stat3.text = "Tracks found: " + hc.GetTracksCounter().ToString();
            stat4.text = "Hear range: " + hc.GetHearRange().ToString("F2");
        }
        else if (obj.CompareTag("Deer"))
        {
            DeerController dc = obj.GetComponent<DeerController>();

            objectType.text = obj.tag;
            state.text = dc.GetCurrentState();
            stat1.text = "Time alive: " + dc.GetTimeAlive().ToString();
            stat2.text = "Tracks left: " + dc.GetTracksLeftCount().ToString();
            stat3.text = "";
            stat4.text = "";
        }
    }


    /// <summary>
    /// Compute minimum value from selected column values in deerDataList.
    /// </summary>
    /// <param name="tableValue">column</param>
    /// <returns>minimum value</returns>
    private string Minimum(int tableValue)
    {
        if (deerDataList.Count.Equals(0))
        {
            return "None";
        }

        float min = float.MaxValue;

        for (int i = 0; i < deerDataList.Count; i++)
        {
            if (deerDataList[i][tableValue] < min)
            {
                min = deerDataList[i][tableValue];
            }
        }
        return min.ToString();
    }


    /// <summary>
    /// Compute maximum value from selected column values in deerDataList.
    /// </summary>
    /// <param name="tableValue">column</param>
    /// <returns>maximum value</returns>
    private string Maximum(int tableValue)
    {
        if (deerDataList.Count.Equals(0))
        {
            return "None";
        }

        float max = float.MinValue;

        for (int i = 0; i < deerDataList.Count; i++)
        {
            if (deerDataList[i][tableValue] > max)
            {
                max = deerDataList[i][tableValue];
            } 
        }
        return max.ToString();
    }


    /// <summary>
    /// Saves statistics for survey purposes to PlayerPrefs string "Statistics".
    /// </summary>
    private void SaveStatistics()
    {
        HunterController hunter;
        hunter = GameObject.FindGameObjectWithTag("Hunter").GetComponent<HunterController>();

        string statistics = "";

        // Hunter stats
        statistics += "Hunter kills: " + hunter.GetKillsCounter() + "\n";
        statistics += "Hunter tracks found: " + hunter.GetTracksCounter() + "\n";
        statistics += "Hunter heard: " + hunter.GetHearingCounter() + "\n";
        statistics += "Hunter's average hear range: " + CountAverage(1) + "\n";

        // Deer stats
        statistics += "Deers average lifetime: " + CountAverage(0) + "\n";
        statistics += "Shortest life: " + Minimum(0) + "\n";
        statistics += "Longest life: " + Maximum(0) + "\n";
        statistics += "All tracks left: " + CountSum(2) + "\n";
        statistics += "Average tracks left: " + CountAverage(2) + "\n";
        
        // Start parameters
        statistics += "Deers amount: " + PlayerPrefs.GetFloat("Deers amount") + "\n";
        statistics += "Trees amount: " + PlayerPrefs.GetFloat("Trees amount") + "\n";
        statistics += "Simulation time: " + simulationTime + " s\n";
        statistics += "Deers starting speed: " + PlayerPrefs.GetFloat("Deers starting speed") + "\n";
        statistics += "Hunter starting speed: " + PlayerPrefs.GetFloat("Starting speed") + "\n";
        statistics += "Hunter reload time: " + PlayerPrefs.GetFloat("Reload time") + " s\n";
        statistics += "Hunter regress time: " + PlayerPrefs.GetFloat("Regress time") + " s\n";
        statistics += "Hunter hear range bonus: " + PlayerPrefs.GetFloat("Hear range bonus") + "\n";
        statistics += "Hunter hear range punish: " + PlayerPrefs.GetFloat("Hear range punish");

        PlayerPrefs.SetString("Statistics", statistics);
    }


    /// <summary>
    /// Load scene by it's ID.
    /// </summary>
    /// <param name="sceneID">Scene's ID</param>
    public void LoadScene(int sceneID)
    {
        SceneManager.LoadScene(sceneID);
    }
}