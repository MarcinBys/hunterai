﻿using UnityEngine;

public class TrackDestroyer : MonoBehaviour {

    public float lifetime;                  // track's prefab lifetime before destroy
    public float timer;                     // used to check which track is older (when destroying the older one)

    public bool trackFound;                 // check if hunter already found this track
    
	void Start ()
    {
        trackFound = false;                 // set as not already found at start

        timer = 0;                          // set start time
        Destroy(gameObject, lifetime);      // destroy track when his lifetime expires
	}

    void Update()
    {
        timer += Time.time;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // delete existing track in the same position, older track is destroyed
        if (other.gameObject.CompareTag("Track") && (other.gameObject.GetComponent<TrackDestroyer>().timer > timer) )
        {
            Destroy(other.gameObject);
        }
    }
}